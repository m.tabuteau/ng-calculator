import { Component, OnInit, OnDestroy } from "@angular/core";
import { CalculatorService } from "./calculator.service";
import { Subscription } from "rxjs";
import { ButtonLayoutService } from "./button-layout.service";
import { ButtonLayout } from "./button-layout.model";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit, OnDestroy {
  calculatorCurrentSub: Subscription;
  calculatorErrorSub: Subscription;
  screen: string = "0";
  error: string = "";
  title = "calculator";
  // TODO: highlight le dernier bouton appuyé (ou uniquement pour un opérateur)
  buttons: ButtonLayout[];

  constructor(
    private calculatorService: CalculatorService,
    private buttonLayoutService: ButtonLayoutService
  ) {}

  ngOnInit() {
    this.buttons = this.buttonLayoutService.getLayout();

    this.calculatorCurrentSub = this.calculatorService.display.subscribe(
      (value: string) => {
        this.error = "";
        this.screen = value;
      }
    );
    this.calculatorErrorSub = this.calculatorService.error.subscribe(
      (errorMessage: string) => {
        this.error = errorMessage;
      }
    );
  }

  ngOnDestroy() {
    this.calculatorCurrentSub.unsubscribe();
    this.calculatorErrorSub.unsubscribe();
  }
}
