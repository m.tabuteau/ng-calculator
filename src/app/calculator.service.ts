import { Injectable } from "@angular/core";
import { Subject, timer } from "rxjs";

@Injectable({ providedIn: "root" })
export class CalculatorService {
  private readonly maxValue = 100000000;
  private leftOperand = 0;
  private rightOperand = 0;
  private operation: Operation = Operation.Add;

  private operationsFunc: {
    [key: number]: (left: number, right: number) => number;
  } = {};

  constructor() {
    this.initOperations();
  }

  display = new Subject<string>();
  error = new Subject<string>();

  inputDigit(value: number) {
    const newValue = this.rightOperand * 10 + value;
    if (newValue >= this.maxValue) return;

    this.rightOperand = newValue;
    this.display.next(this.rightOperand.toString());
  }

  add() {
    this.equals();
    this.operation = Operation.Add;
  }

  multiply() {
    this.equals();
    this.operation = Operation.Multiply;
  }

  divide() {
    this.equals();
    this.operation = Operation.Divide;
  }

  minus() {
    this.equals();
    this.operation = Operation.Substract;
  }

  equals() {
    if (this.operation !== Operation.None) {
      try {
        const newValue = this.operationsFunc[this.operation](
          this.leftOperand,
          this.rightOperand
        );

        this.rightOperand = 0;
        this.leftOperand = newValue;
        this.display.next(newValue.toString());
        this.operation = Operation.None;
      } catch (errorMessage) {
        this.error.next(errorMessage);
        this.operation = Operation.None;

        timer(1000).subscribe(() =>
          this.display.next(this.leftOperand.toString())
        );
      }
    }
  }

  private initOperations() {
    this.operationsFunc[Operation.Add] = (left, right) => left + right;
    this.operationsFunc[Operation.Substract] = (left, right) => left - right;
    this.operationsFunc[Operation.Divide] = (left, right) => {
      if (right === 0) throw "Error division by zero";
      return left / right;
    };
    this.operationsFunc[Operation.Multiply] = (left, right) => left * right;
  }
}

enum Operation {
  None,
  Add,
  Substract,
  Multiply,
  Divide
}
