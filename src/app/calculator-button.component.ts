import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "calc-button",
  template: `
    <button class="button">{{ text }}</button>
  `,
  styles: []
})
export class CalculatorButtonComponent {
  @Input() text: string = "";
}
