import { Injectable } from "@angular/core";
import { CalculatorService } from "./calculator.service";
import { ColumnClass, ButtonLayout } from "./button-layout.model";

@Injectable({
  providedIn: "root"
})
export class ButtonLayoutService {
  constructor(private calculatorService: CalculatorService) {}

  getLayout(): ButtonLayout[] {
    return [
      { text: "AC", columnClass: "is-one-quarter" },
      { text: "+/-", columnClass: "is-one-quarter" },
      { text: "%", columnClass: "is-one-quarter" },
      {
        text: "/",
        columnClass: "is-one-quarter",
        onClick: () => this.calculatorService.divide()
      },
      this.digitButtonLayout(7, ColumnClass.OneQuarter),
      this.digitButtonLayout(8, ColumnClass.OneQuarter),
      this.digitButtonLayout(9, ColumnClass.OneQuarter),
      {
        text: "x",
        columnClass: "is-one-quarter",
        onClick: () => this.calculatorService.multiply()
      },
      this.digitButtonLayout(4, ColumnClass.OneQuarter),
      this.digitButtonLayout(5, ColumnClass.OneQuarter),
      this.digitButtonLayout(6, ColumnClass.OneQuarter),
      {
        text: "-",
        columnClass: "is-one-quarter",
        onClick: () => this.calculatorService.minus()
      },
      this.digitButtonLayout(1, ColumnClass.OneQuarter),
      this.digitButtonLayout(2, ColumnClass.OneQuarter),
      this.digitButtonLayout(3, ColumnClass.OneQuarter),
      {
        text: "+",
        columnClass: "is-one-quarter",
        onClick: () => this.calculatorService.add()
      },
      this.digitButtonLayout(0, ColumnClass.Half),
      { text: ",", columnClass: "is-one-quarter" },
      {
        text: "=",
        columnClass: "is-one-quarter",
        onClick: () => this.calculatorService.equals()
      }
    ];
  }

  private digitButtonLayout(
    digit: number,
    columnClass: ColumnClass
  ): ButtonLayout {
    return {
      text: digit.toString(),
      columnClass: columnClass,
      onClick: () => this.calculatorService.inputDigit(digit)
    };
  }
}
