export enum ColumnClass {
  OneQuarter = "is-one-quarter",
  Half = "is-half"
}

export interface ButtonLayout {
  text: string;
  columnClass: ColumnClass | string;
  onClick?: () => void;
}
